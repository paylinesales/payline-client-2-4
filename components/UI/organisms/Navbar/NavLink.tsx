import Link from "next/link";
import React from "react";
import { useRouter } from "next/dist/client/router";

interface INavLinkProps {
  href: string;
  children: React.ReactNode;
  onClick?: (event: React.MouseEvent<HTMLDivElement>) => void;
  className?: string;
}

const NavLink: React.FC<INavLinkProps> = ({
  href,
  children,
  onClick,
  className: classes,
}) => {
  const router = useRouter();
  return (
    <Link href={href}>
      <div
        className={`cursor-pointer border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700 inline-flex items-center min-w-max text-sm ${
          router?.asPath === href ? "font-medium" : ""
        } ${classes}`}
        onClick={onClick}>
        {children}
      </div>
    </Link>
  );
};

NavLink.defaultProps = {
  onClick() {
    // eslint-disable-next-line no-console
    console.log("Clicked");
  },
  className: "",
};

export default NavLink;
