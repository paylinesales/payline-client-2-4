import React from "react";
import NavLink from "./NavLink";

export interface NavMenuArrProps {
  name: string;
  href: string;
}

const NavMenu: React.FC<{
  menu: NavMenuArrProps[];
  className?: string;
  toggleDropdownMenu?: (event: React.MouseEvent<HTMLDivElement>) => void;
  isDropdownMenu?: boolean;
}> = (props) => {
  const {
    menu,
    className: classes,
    children,
    toggleDropdownMenu,
    isDropdownMenu,
  } = props;
  const linkClass = isDropdownMenu ? "font-medium" : "";
  return (
    <nav className={classes}>
      {menu.map(({ href, name }) => {
        if (name === "Solutions") {
          return (
            <NavLink
              key={name}
              href={href}
              onClick={toggleDropdownMenu}
              className={linkClass}>
              <span className="uppercase">{name}</span>
            </NavLink>
          );
        }
        return (
          <NavLink key={name} href={href}>
            <span className="uppercase">{name}</span>
          </NavLink>
        );
      })}
      {children}
    </nav>
  );
};

NavMenu.defaultProps = {
  className: "",
  toggleDropdownMenu() {
    // eslint-disable-next-line no-console
    console.log("toggle dropdown");
  },
  isDropdownMenu: false,
};

export default NavMenu;
