import React from "react";
import ButtonLink from "../../molecules/buttonLink";

interface Props {
  lead: string | null;
  bold: string | null;
  end: string | null;
}

const CTABanner: React.FC<Props> = ({ lead, bold, end }) => {
  return (
    <div className="bg-payline-cta text-payline-white flex p-2.5 justify-center items-center h-14 md:p-2">
      <p className="font-san font-thin">
        {lead}
        <span className="font-opensans text-center font-extrabold text-payline-white uppercase">
          {` ${bold} `}
        </span>
        {end}
      </p>
      <div className="hidden md:block m-0">
        <ButtonLink
          className="uppercase text-sm font-bold mx-7 px-3 py-0.5 font-sans bg-white text-payline-dark"
          href="https://paylinedata.com/apply-now-ue/">
          Apply Now
        </ButtonLink>
      </div>
    </div>
  );
};

export default CTABanner;
