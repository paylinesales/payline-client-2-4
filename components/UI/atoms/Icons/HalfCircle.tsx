import React from "react";

const HalfCircle: React.FC<{ className?: string }> = (props) => {
  const { className: classes } = props;
  return (
    <svg
      className={classes}
      width="26"
      height="26"
      viewBox="0 0 26 26"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M21 13C21 8.58172 17.4183 5 13 5C8.58172 5 5 8.58172 5 13"
        fill="#FBBD92"
      />
      <circle
        cx="13"
        cy="13"
        r="7.5"
        transform="rotate(90 13 13)"
        stroke="#FBBD92"
      />
      <circle
        cx="13"
        cy="13"
        r="12.5"
        transform="rotate(-180 13 13)"
        stroke="#FBBD92"
      />
    </svg>
  );
};

HalfCircle.defaultProps = {
  className: "",
};

export default HalfCircle;
