import React, { ReactElement } from "react";

export interface IButtonProps {
  buttonText: string | React.ComponentProps<"svg">;
  onClick(e);
}

function Button({ buttonText, onClick }: IButtonProps): ReactElement {
  return (
    <button
      type="button"
      onClick={onClick}
      className="inline-flex items-center px-6 py-3 border border-transparent text-base font-medium rounded-full shadow-sm bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
      {buttonText}
    </button>
  );
}

export default Button;
