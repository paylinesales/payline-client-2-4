import React, { useState } from "react";

interface AccordionProps {
  title: React.ReactNode;
  content: React.ReactNode;
}

export const Accordion: React.FC<AccordionProps> = ({ title, content }) => {
  const [active, setActive] = useState(false);

  const toggleAccordion = () => {
    setActive(!active);
  };

  return (
    <div className="">
      <div
        className={`${
          active ? "text-payline-black" : ""
        } bg-payline-background-light p-2 my-1 rounded box-border cursor-pointer flex items-center justify-between`}
        onClick={toggleAccordion}>
        <p className="inline-block text-footnote light">{title}</p>
        {active ? "-" : "+"}
      </div>
      {active ? (
        <div className="overflow-auto transition duration-700 ease-in-out">
          {content}
        </div>
      ) : null}
    </div>
  );
};

export default Accordion;
