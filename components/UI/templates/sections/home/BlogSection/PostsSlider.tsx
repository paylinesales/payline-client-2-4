import React, { useRef } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
// import Swiper core and required modules
import SwiperCore, { Navigation } from "swiper/core";
import PostCard from "./PostCard";
import PostsSliderNav from "./PostsSliderNav";

// Activate Swiper modules
SwiperCore.use([Navigation]);

const getThumbnailSize = (imageUrl: string) => {
  const thumbnailSize = "150x150";
  const imageFormatDotPos = imageUrl?.lastIndexOf(".");
  const beforeDot = imageUrl?.substr(0, imageFormatDotPos);
  const afterDot = imageUrl?.substr(imageFormatDotPos, imageUrl?.length - 1);
  return `${beforeDot}-${thumbnailSize}${afterDot}`;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const PostSlider: React.FC<{ posts: any }> = (props) => {
  const { posts } = props;
  const { edges } = posts;
  const navPrevRef = useRef<HTMLDivElement>(null);
  const navNextRef = useRef<HTMLDivElement>(null);
  return (
    <div className="slider-container relative">
      <img
        src="/images/blog-shape.png"
        width={583}
        height={376}
        className="absolute top-10 -left-1/4"
      />
      <Swiper
        spaceBetween={35}
        slidesPerView="auto"
        freeMode={false}
        navigation={{
          prevEl: navPrevRef.current ? navPrevRef.current : undefined,
          nextEl: navNextRef.current ? navNextRef.current : undefined,
        }}
        onInit={(swiper) => {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          // eslint-disable-next-line no-param-reassign
          swiper.params.navigation.prevEl = navPrevRef.current;
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          // eslint-disable-next-line no-param-reassign
          swiper.params.navigation.nextEl = navNextRef.current;
          swiper.navigation.update();
        }}>
        {edges.map((post) => (
          <SwiperSlide
            key={post?.node?.id}
            className="post-slide shadow-lg hover:shadow-xl transition-shadow duration-300 rounded-b-md my-8 bg-payline-white"
            tag="article">
            <PostCard
              key={post?.node?.id}
              imgSrc={getThumbnailSize(
                post?.node?.featuredImage?.node?.mediaItemUrl,
              )}
              unformattedDate={post?.node?.date}
              title={post?.node?.title}
              slug={post?.node?.slug}
            />
          </SwiperSlide>
        ))}
        <PostsSliderNav navPrevRef={navPrevRef} navNextRef={navNextRef} />
      </Swiper>
    </div>
  );
};

export default PostSlider;
