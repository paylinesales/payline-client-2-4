import React from "react";

import WhiteButton from "@/components/UI/atoms/WhiteButton";

const Tags: React.FC<{ className?: string; tags: string[] }> = (props) => {
  const { tags, className: classes } = props;
  return (
    <div
      className={`my-6 hidden lg:flex flex-wrap justify-between gap-5 ${classes}`}>
      {tags.map((a) => {
        return (
          <WhiteButton
            key={a}
            className="rounded-full shadow-sm px-8 font-semibold flex-grow"
            disabled={false}>
            {a}
          </WhiteButton>
        );
      })}
    </div>
  );
};

Tags.defaultProps = {
  className: "",
};

export default Tags;
