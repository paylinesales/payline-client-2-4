/* eslint-disable react/button-has-type */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable  react/require-default-props, react/no-unused-prop-types, react/forbid-prop-types */
import Image from "next/image";
import Link from "next/link";
import React, { ReactElement, useState } from "react";
import { ApolloError, useApolloClient } from "@apollo/client";
import loadMoreQuery from "../../../../../queries/load-more";
import BlogPost, { BlogPostType } from "../../../atoms/BlogPost";

interface categoryCursorType {
  [key: string]: string;
}

interface queryResultType {
  loading: boolean | null;
  error: ApolloError | undefined | null;
  data: any | null;
}

interface IBlogArticlesSectionProps {
  categories?: Array<string>;
  setCategories?: React.Dispatch<React.SetStateAction<string[]>>;
  search?: string;
  queryResult: queryResultType;
}

const BlogArticlesSection: React.FC<IBlogArticlesSectionProps> = (
  props,
): ReactElement => {
  const { categories, setCategories, search, queryResult } = props;
  const { loading, error, data } = queryResult;
  const client = useApolloClient();
  const [totalPosts, setTotalPosts] = useState(10);
  if (loading) {
    return (
      <div className="py-12 md:px-8 md:bg-blog-search-oval md:bg-cover md:bg-no-repeat">
        <b>
          <h1 className="px-4 text-4xl text-payline-black">Loading...</h1>
        </b>
      </div>
    );
  }
  if (error) {
    return (
      <div className="py-12 md:px-8 md:bg-blog-search-oval md:bg-cover md:bg-no-repeat">
        <b>
          <h1 className="px-4 text-4xl text-payline-black">Error! {error}</h1>
        </b>
      </div>
    );
  }
  if (data) {
    const posts: Array<BlogPostType> = [];
    const categoryCursors: Array<categoryCursorType> = [];
    data.categories.nodes.forEach((category) => {
      const { id } = category;
      const cursor = category.posts.pageInfo.endCursor;
      const categoryCursorHolder: categoryCursorType = {
        id,
        cursor,
      };
      categoryCursors.push(categoryCursorHolder);
      category.posts.nodes.forEach((post) => {
        const blogPost = post.id;
        if (!(posts.indexOf(blogPost) > -1)) {
          posts.push(blogPost);
        }
      });
    });
    return (
      <div className="py-12 md:px-8 md:bg-blog-search-oval md:bg-cover md:bg-no-repeat">
        {posts.slice(0, totalPosts).map((post) => (
          <BlogPost
            post={post}
            setCategories={setCategories}
            categories={categories}
          />
        ))}
        <div className="flex justify-center gap-2">
          {/* todo: link and fix query */}
          {posts.length > totalPosts && (
            // eslint-disable-next-line no-return-assign
            <div
              onClick={() => {
                setTotalPosts(totalPosts + 10);
                if (posts.length <= totalPosts) {
                  categoryCursors.forEach((cursorInfo) => {
                    const { id, cursor } = cursorInfo;
                    const addPosts = async (event) => {
                      const results = await client.query({
                        query: loadMoreQuery,
                        variables: { id, search, cursor },
                      });
                      if (results.data) {
                        results.data.category.posts.nodes.forEach((post) => {
                          const blogPost = post.id;
                          if (!(posts.indexOf(blogPost) > -1)) {
                            posts.push(blogPost);
                          }
                        });
                      }
                    };
                  });
                }
              }}>
              <button>
                <div className="text-payline-black font-bold text-lg">
                  Load More
                </div>
                <Image
                  src="/images/svg/arrow-down-black.svg"
                  width={14}
                  height={16}
                />
              </button>
            </div>
          )}
        </div>
      </div>
    );
  }
  return (
    <div className="py-12 md:px-8 md:bg-blog-search-oval md:bg-cover md:bg-no-repeat">
      <b>
        <h1 className="px-4 text-4xl text-payline-black">
          Enter a search to begin!
        </h1>
      </b>
    </div>
  );
};

export default BlogArticlesSection;
